<?php

return [
    'news_manager' => 'Gestor de noticias',
    'name' => 'Nombre',
    'date' => 'Fecha',
    'status' => 'Estado',
    'title' => 'Título',
    'featured' => 'Destacado',
    'image' => 'Imagen principal',
    'content' => 'Contenido',
    'intro' => 'Introducción',
    'slug' => 'Slug (URL)',
    'slug_hint' => 'Se generará automáticamente a partir del nombre o título, si se deja vacío.',
    'parent' => 'Padre',
    'category_singular' => 'Categoría',
    'category_plural' => 'Categorías',
    'article_singular' => 'Artículo',
    'article_plural' => 'Artículos',
    'tag_singular' => 'Etiqueta',
    'tag_plural' => 'Etiquetas',
    'PUBLISHED' => 'Publicado',
    'DRAFT' => 'Borrador',
];
