<?php

return [
    'news_manager' => 'News Manager',
    'name' => 'Name',
    'date' => 'Date',
    'status' => 'Status',
    'title' => 'Title',
    'featured' => 'Featured',
    'image' => 'Main image',
    'content' => 'Content',
    'intro' => 'Intro',
    'slug' => 'Slug (URL)',
    'slug_hint' => 'Will be automatically generated from the name, if left empty.',
    'parent' => 'Parent',
    'category_singular' => 'Category',
    'category_plural' => 'Categories',
    'article_singular' => 'Article',
    'article_plural' => 'Articles',
    'tag_singular' => 'Tag',
    'tag_plural' => 'Tags',
    'PUBLISHED' => 'Published',
    'DRAFT' => 'Draft',
];
