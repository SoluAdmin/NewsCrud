@module("SoluAdmin\\NewsCrud")
<li class="treeview">
    <a href="#"><i class="fa fa-newspaper-o"></i> <span>{{trans('SoluAdmin::NewsCrud.news_manager')}}</span> <i
                class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.NewsCrud.route_prefix', '') . '/category') }}">
                <i class="fa fa-th-list"></i> <span>{{trans('SoluAdmin::NewsCrud.category_plural')}}</span>
            </a>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.NewsCrud.route_prefix', '') . '/tag') }}">
                <i class="fa fa-tags"></i> <span>{{trans('SoluAdmin::NewsCrud.tag_plural')}}</span>
            </a>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.NewsCrud.route_prefix', '') . '/article') }}">
                <i class="fa fa-newspaper-o"></i> <span>{{trans('SoluAdmin::NewsCrud.article_plural')}}</span>
            </a>
        </li>
    </ul>
</li>
@endmodule