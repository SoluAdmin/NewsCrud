<?php

return [
    'route_prefix' => '/news-manager',
    'middleware' => false,
    'setup_routes' => false,
    'publishes_migrations' => false,
    'tables_prefix' => 'blog_',
];
