<?php

namespace SoluAdmin\NewsCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ArticleRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'title' => 'required|min:2|max:255',
            'slug' => 'unique:' . config('SoluAdmin.NewsCrud.tables_prefix') . 'articles,slug,' . \Request::get('id'),
            'content' => 'required|min:2',
            'date' => 'required|date',
            'status' => 'required',
        ];
    }
}
