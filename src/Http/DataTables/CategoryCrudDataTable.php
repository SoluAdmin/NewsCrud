<?php

namespace SoluAdmin\NewsCrud\Http\DataTables;

use SoluAdmin\NewsCrud\Models\Category;
use SoluAdmin\Support\Interfaces\DataTable;

class CategoryCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::NewsCrud.name'),
            ],
            [
                'name' => 'slug',
                'label' => trans('SoluAdmin::NewsCrud.slug'),
            ],
            [
                'label' => trans('SoluAdmin::NewsCrud.parent'),
                'type' => 'select',
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => 'name',
                'model' => Category::class,
            ],
        ];
    }
}
