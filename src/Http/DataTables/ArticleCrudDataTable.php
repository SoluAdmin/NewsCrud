<?php

namespace SoluAdmin\NewsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class ArticleCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'date',
                'label' => trans('SoluAdmin::NewsCrud.date'),
                'type' => 'date',
            ],
            [
                'name' => 'status',
                'label' => trans('SoluAdmin::NewsCrud.status'),
                'type' => 'news_crud_enum',
            ],
            [
                'name' => 'title',
                'label' => trans('SoluAdmin::NewsCrud.title'),
            ],
            [
                'name' => 'featured',
                'label' => trans('SoluAdmin::NewsCrud.featured'),
                'type' => 'check',
            ],
        ];
    }
}
