<?php

namespace SoluAdmin\NewsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class TagCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::NewsCrud.name'),
            ],
            [
                'name' => 'slug',
                'label' => trans('SoluAdmin::NewsCrud.slug'),
            ],
        ];
    }
}
