<?php

namespace SoluAdmin\NewsCrud\Http\Controllers;

use SoluAdmin\NewsCrud\Http\Requests\CategoryRequest as UpdateRequest;
use SoluAdmin\NewsCrud\Http\Requests\CategoryRequest as StoreRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class CategoryCrudController extends BaseCrudController
{
    public function setUp()
    {
        parent::setup();
        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('name', 0);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
