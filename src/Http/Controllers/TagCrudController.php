<?php

namespace SoluAdmin\NewsCrud\Http\Controllers;

use SoluAdmin\NewsCrud\Http\Requests\TagRequest as UpdateRequest;
use SoluAdmin\NewsCrud\Http\Requests\TagRequest as StoreRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class TagCrudController extends BaseCrudController
{
    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
