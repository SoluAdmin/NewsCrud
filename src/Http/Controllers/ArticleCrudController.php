<?php

namespace SoluAdmin\NewsCrud\Http\Controllers;

use SoluAdmin\NewsCrud\Http\Requests\ArticleRequest as UpdateRequest;
use SoluAdmin\NewsCrud\Http\Requests\ArticleRequest as StoreRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class ArticleCrudController extends BaseCrudController
{
    public function setUp()
    {
        parent::setup();
        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
