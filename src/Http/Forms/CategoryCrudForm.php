<?php

namespace SoluAdmin\NewsCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class CategoryCrudForm implements Form
{

    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::NewsCrud.category_singular'),
            ],
            [
                'name' => 'slug',
                'label' => trans('SoluAdmin::NewsCrud.slug'),
                'type' => 'text',
                'hint' => trans('SoluAdmin::NewsCrud.slug_hint'),
            ],
        ];
    }
}
