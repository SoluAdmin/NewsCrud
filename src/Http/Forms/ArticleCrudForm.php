<?php

namespace SoluAdmin\NewsCrud\Http\Forms;

use SoluAdmin\NewsCrud\Models\Category;
use SoluAdmin\NewsCrud\Models\Tag;
use SoluAdmin\Support\Interfaces\Form;

class ArticleCrudForm implements Form
{

    public function fields()
    {
        return [
            [
                'name' => 'title',
                'label' => trans('SoluAdmin::NewsCrud.title'),
                'placeholder' => trans('SoluAdmin::NewsCrud.title_placeholder'),
            ],
            [
                'name' => 'slug',
                'label' => trans('SoluAdmin::NewsCrud.slug'),
                'type' => 'text',
                'hint' => trans('SoluAdmin::NewsCrud.slug_hint'),
            ],
            [
                'name' => 'date',
                'label' => trans('SoluAdmin::NewsCrud.date'),
                'type' => 'datetime_picker',
                'date_picker_options' => [
                    'todayBtn' => true,
                    'format' => 'dd-mm-yyyy',
                    'language' => 'es'
                ]
            ],
            [
                'name' => 'image',
                'label' => trans('SoluAdmin::NewsCrud.image'),
                'type' => 'browse'
            ],
            [
                'name' => 'intro',
                'label' => trans('SoluAdmin::NewsCrud.intro'),
                'type' => 'ckeditor'
            ],
            [
                'name' => 'content',
                'label' => trans('SoluAdmin::NewsCrud.content'),
                'type' => 'ckeditor'
            ],
            [
                'label' => trans('SoluAdmin::NewsCrud.category_plural'),
                'type' => 'select2_multiple',
                'name' => 'categories',
                'entity' => 'category',
                'attribute' => 'name',
                'model' => Category::class,
                'pivot' => true,
            ],
            [
                'label' => trans('SoluAdmin::NewsCrud.tag_plural'),
                'type' => 'select2_multiple',
                'name' => 'tags',
                'entity' => 'tag',
                'attribute' => 'name',
                'model' => Tag::class,
                'pivot' => true,
            ],
            [
                'name' => 'status',
                'label' => trans('SoluAdmin::NewsCrud.status'),
                'type' => 'news_crud_enum'
            ],
            [
                'name' => 'featured',
                'label' => trans('SoluAdmin::NewsCrud.featured'),
                'type' => 'checkbox'
            ],
        ];
    }
}
