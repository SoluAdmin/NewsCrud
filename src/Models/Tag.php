<?php

namespace SoluAdmin\NewsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use SoluAdmin\Support\Traits\SafeSluggable;
use Illuminate\Database\Eloquent\Model;
use SoluAdmin\Support\Traits\SafeSluggableScopeHelpers;

class Tag extends Model
{
    use CrudTrait;
    use HasTranslations;
    use SafeSluggable;
    use SafeSluggableScopeHelpers;

    protected $primaryKey = 'id';
    protected $fillable = ['name', 'slug'];
    protected $translatable = ['name', 'slug'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('SoluAdmin.NewsCrud.tables_prefix') . 'tags');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, config('SoluAdmin.NewsCrud.tables_prefix') . 'article_tag');
    }

    public function getSlugOrNameAttribute()
    {
        return ($this->slug != '') ? $this->slug : $this->name;
    }
}
