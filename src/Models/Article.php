<?php

namespace SoluAdmin\NewsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use SoluAdmin\Support\Traits\SafeSluggableScopeHelpers;
use SoluAdmin\Support\Traits\SafeSluggable;

class Article extends Model
{
    use CrudTrait;
    use HasTranslations;
    use SafeSluggable;
    use SafeSluggableScopeHelpers;

    protected $primaryKey = 'id';
    protected $fillable = ['title', 'slug', 'content', 'image', 'status', 'date', 'featured'];
    protected $translatable = ['title', 'slug', 'content', 'image'];
    protected $casts = [
        'featured' => 'boolean',
        'date' => 'datetime',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('SoluAdmin.NewsCrud.tables_prefix') . 'articles');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, config('SoluAdmin.NewsCrud.tables_prefix') . 'article_category');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, config('SoluAdmin.NewsCrud.tables_prefix') . 'article_tag');
    }

    public function scopePublished($query)
    {
        return $query->where('status', 'PUBLISHED')
            ->where('date', '<=', date('Y-m-d'))
            ->orderBy('date', 'DESC');
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', true)
            ->where('date', '<=', Carbon::now()->toDateTimeString())
            ->orderBy('date', 'DESC');
    }

    public function getSlugOrTitleAttribute()
    {
        return ($this->slug != '') ? $this->slug : $this->title;
    }
}
