<?php

namespace SoluAdmin\NewsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use SoluAdmin\Support\Traits\SafeSluggableScopeHelpers;
use SoluAdmin\Support\Traits\SafeSluggable;

class Category extends Model
{

    use CrudTrait;
    use HasTranslations;
    use SafeSluggable;
    use SafeSluggableScopeHelpers;

    protected $primaryKey = 'id';
    protected $fillable = ['name', 'slug', 'parent_id'];
    protected $translatable = ['name', 'slug'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('SoluAdmin.NewsCrud.tables_prefix') . 'categories');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, config('SoluAdmin.NewsCrud.tables_prefix') . 'article_category');
    }

    public function scopeFirstLevelItems($query)
    {
        return $query->where('depth', '1')
            ->orWhere('depth', null)
            ->orderBy('lft', 'ASC');
    }

    public function getSlugOrNameAttribute()
    {
        return ($this->slug != '') ? $this->slug : $this->name;
    }
}
