<?php

namespace SoluAdmin\NewsCrud\Providers;

use SoluAdmin\Support\Helpers\PublishableAssets as Assets;
use SoluAdmin\Support\Providers\CrudServiceProvider;

class NewsCrudServiceProvider extends CrudServiceProvider
{

    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
        Assets::MODULE_MIGRATIONS,
    ];

    protected $resources = [
        'category' => 'CategoryCrudController',
        'tag' => 'TagCrudController',
        'article' => 'ArticleCrudController',
    ];

    protected function bootExtras()
    {
        $this->publishes([__DIR__. '/../../resources/extras/views' => base_path('resources/views')], 'views');
    }
}
